class HomeController < ApplicationController
  def index
  end

  def new_home
    render component: "NewWorld"
  end

  def owner
  end

  def lifecycle
  end
end
