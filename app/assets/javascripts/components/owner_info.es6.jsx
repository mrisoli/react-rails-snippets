class OwnerInfo extends React.Component {
  constructor() {
    super();
    this.state=({label: 'success'})
    this.toggle = this.toggle.bind(this)
  }

  toggle(){
    if(this.state.label == 'success')
      this.setState({label: 'danger'})
    else
      this.setState({label: 'success'})
  }
  render () {
    return (
      <div className='col-sm-12'>
        <Widget label={this.state.label} toggle={this.toggle} />
        <Widget label={this.state.label} toggle={this.toggle} />
        <Widget label={this.state.label} toggle={this.toggle} />
        <Widget label={this.state.label} toggle={this.toggle} />
        <a href="/lifecycle"><button type="button" className="btn btn-lg btn-info">Next</button></a>
      </div>
    );
  }
}

const Widget = (props) => {
  return(
    <div>
      <button type="button" className={`btn btn-lg btn-${props.label}`} onClick={props.toggle}>Toggle</button>
      <br />
      <br />
    </div>
  );
}
