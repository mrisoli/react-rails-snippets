Rails.application.routes.draw do
  resources :info, only: :show do
    member do
      get :state
      get :owner
    end
  end

  root 'home#index'

  get 'home/index'
  get 'page2', to: 'home#new_home'
  get 'toggle', to: 'home#owner'
  get 'lifecycle', to: 'home#lifecycle'
end
