class Lifecycle extends React.Component {
  constructor() {
    super();
    this.state = { times: 0};
    this.update = this.update.bind(this);
  }


  update(){
    this.setState({times: this.state.times + 1});
  }

  componentWillMount(){
    console.log("Mounting!");
  }
  render () {
    console.log("Rendering!");
    return (
      <div className='col-sm-12'>
        <button className='btn btn-lg btn-warning' style={styles} onClick={this.update}>{this.state.times}</button>
      </div>
    );
  }
  componentDidMount(){
    console.log("Mounted!");
  }
}

const styles = {margin: '250px', padding: '50px', fontSize: '75px'};
