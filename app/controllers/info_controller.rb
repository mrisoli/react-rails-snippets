class InfoController < ApplicationController

  before_action :set_info

  def show
    render component: "Info", props: {header: @info.header, text: @info.body, repeatText: params[:repeat].to_i}
  end

  def state
  end

  private

  def set_info
    @info = Info.find(params[:id])
  end
end
