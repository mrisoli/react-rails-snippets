class GetInfo extends React.Component {
  render () {
    return (
      <a href={`/info/${this.props.id}?repeat=${this.props.repeat}`}><button type="button" className="btn btn-lg btn-info">Next</button></a>
    );
  }
}

GetInfo.defaultProps = {
  repeat: 1
}
GetInfo.propTypes = {
  id: React.PropTypes.number,
  repeat: React.PropTypes.number
};
