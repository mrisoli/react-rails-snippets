class Info extends React.Component {

  render () {
    return (
      <div className='col-sm-12'>
        <h1>{this.props.header}</h1>
          {[...Array(this.props.repeatText)].map( x => <p>{this.props.text}</p>)}
        <br/>
        <GetInfo id={1} repeat={5}/>
        <br />
        <br />
        <a href={`/info/${1}/state`}><button type="button" className="btn btn-lg btn-info">Info with state</button></a>
      </div>
    );
  }
}

Info.defaultProps = {
  repeatText: 1
}
Info.propTypes = {
  header: React.PropTypes.string.isRequired,
  text: React.PropTypes.string.isRequired,
  repeatText: React.PropTypes.number
};
