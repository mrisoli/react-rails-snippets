class StateInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      repeat: 1
    }
  }

  update(e) {
    this.setState({repeat: this.state.repeat + 1})
  }
  render () {
    return (
      <div className='col-sm-12'>
        <h1>{this.props.header}</h1>
        <br />
        <a href='/toggle'><button type="button" className="btn btn-info btn-lg">Next</button></a>
        <br />
        <br />
        <button type="button" className="btn btn-primary btn-lg" onClick={this.update.bind(this)}>Append Text</button>
        <br />
        <br />
        {[...Array(this.state.repeat)].map( x => <p>{this.props.text}</p>)}
      </div>
    );
  }
}

StateInfo.propTypes = {
  header: React.PropTypes.string,
  text: React.PropTypes.string
};
